from django.db import models
from django.conf import settings

from .managers import ReportManager
from core.models import BaseModel


class Report(BaseModel):
    """
    Report model for token and user reports.
    
    """
    account = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    inappropriate_content = models.BooleanField(default=False)
    spam = models.BooleanField(default=False)
    reason = models.TextField(max_length=300)

    objects = ReportManager()

    def __str__(self):
        """ Report name. """
        return self.id

    def save(self, *args, **kwargs):
        super(Report, self).save(*args, **kwargs)

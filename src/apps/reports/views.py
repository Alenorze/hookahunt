from rest_framework import viewsets, permissions

from .models import Report
from core.permissions import IsModerator
from .serializers import ReportSerializer



class ReportViewSet(viewsets.ModelViewSet):
    serializer_class = ReportSerializer
    permission_classes = [IsModerator]
    queryset = Report.objects.all()

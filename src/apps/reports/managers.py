from core.managers import BaseQuerySet, BaseManager



class ReportQuerySet(BaseQuerySet):
    """ Token query set. """
    pass


class ReportManager(BaseManager):
    """
    The token manager using it can easily change the queryset.

    """
    def get_query_set(self):
        return ReportQuerySet(self.model)
    
    def all(self):
        return self.get_queryset()

    def __getattr__(self, attr, *args):
        if attr.startswith("_"):
            raise AttributeError
        return getattr(self.get_query_set(), attr, *args)
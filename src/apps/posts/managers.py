from django.db import models
from django.db.models.query import QuerySet


class ArticleQuerySet(QuerySet):
    """ Article query set. """
    def active(self):
        return self.filter(is_active=True)


class ArticleManager(models.Manager):
    """
    The article manager using it can easily change the queryset.

    """
    def get_query_set(self):
        return ArticleQuerySet(self.model)
    
    def all(self):
        return self.get_queryset()

    def __getattr__(self, attr, *args):
        if attr.startswith("_"):
            raise AttributeError
        return getattr(self.get_query_set(), attr, *args)

from rest_framework import serializers

from core.fields import HyperlinkedSorlImageField
from posts.models import Article, PreviewImage
from accounts.models import Account, AccountImage

from versatileimagefield.serializers import VersatileImageFieldSerializer


class AccountImageSerializer(serializers.ModelSerializer):
    source = VersatileImageFieldSerializer(
        sizes=[
            ('full', 'url'),
            ('thumb', 'thumbnail__100x100')
        ]
    )
    class Meta:
        model = AccountImage
        exclude = ['id', 'image_ppoi']


class AccountSerializer(serializers.ModelSerializer):
    profile_photo = AccountImageSerializer()
    class Meta:
        model = Account
        fields = (
            'username',
            'profile_photo',
            'bio'
        )

class ArticleImageSerializer(serializers.ModelSerializer):
    source = VersatileImageFieldSerializer(
        sizes=[
            ('full', 'url'),
            ('thumb', 'thumbnail__100x100')
        ]
    )
    class Meta:
        model = PreviewImage
        exclude = ['id', 'image_ppoi']


class ArticleSerializer(serializers.ModelSerializer):
    author = AccountSerializer()
    image = ArticleImageSerializer()
    tags = serializers.StringRelatedField(many=True)
    
    class Meta:
        model = Article
        exclude = ['created_at']
   
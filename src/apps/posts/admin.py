from django.contrib import admin

from .models import Article, PreviewImage

admin.site.register(Article)
admin.site.register(PreviewImage)

# Generated by Django 2.0.5 on 2018-05-24 02:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0015_auto_20180524_0236'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='images',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='posts.ArticleImage'),
        ),
    ]

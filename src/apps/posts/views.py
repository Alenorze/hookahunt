from rest_framework import mixins, viewsets
from rest_framework.response import Response

from .models import Article
from .serializers import ArticleSerializer
from core.permissions import IsOwnerOrReadOnly



class ArticleViewSet(mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    viewsets.GenericViewSet):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
    lookup_field = 'slug'

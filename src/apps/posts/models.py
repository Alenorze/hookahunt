from django.db import models
from django.conf import settings
from django.utils.text import slugify

from mptt.models import TreeForeignKey
from versatileimagefield.fields import VersatileImageField

from core.models import BaseModel, BaseImage
from core.constants import ARTICLE_STATUS
from core.mixins import MetaModelMixin
from core.utils import image_path
from .managers import ArticleManager
from categories.models import Category
from tags.models import Tag



class PreviewImage(BaseImage):
    source = VersatileImageField(
        upload_to='images/',
        ppoi_field='image_ppoi',
        null=True
    )
    
    class Meta:
        verbose_name = 'Article Preview'
        verbose_name_plural = 'Article Previews'


class Article(BaseModel, MetaModelMixin):
    title = models.CharField(max_length=90, unique=True)
    description = models.TextField(blank=True) 
    slug = models.SlugField(max_length=90, unique=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name= 'Author',    # One of each admins 
        on_delete=models.CASCADE,
        default='1'
    )
    image = models.OneToOneField(PreviewImage, related_name='articles', on_delete=models.CASCADE, blank=True, null=True)
    body = models.TextField(editable=True, blank=True)
    status = models.CharField(max_length=120, default='Created', choices=ARTICLE_STATUS)
    published_at = models.DateField(auto_now_add=True, blank=True, null=True)
    category = TreeForeignKey(Category, related_name='category', on_delete=models.CASCADE, blank=True, null=True)
    tags = models.ManyToManyField(Tag, blank=True)
    favorited = models.BooleanField(default=False)
    favorites_count = models.PositiveIntegerField(default='0')
   
    objects = ArticleManager()

    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Article'

    def __str__(self):
        return self.title

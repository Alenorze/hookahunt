from django.db import models
from django.db.models import permalink
from django.utils.text import slugify

from mptt.models import MPTTModel



class Category(MPTTModel):
    name = models.CharField(max_length=64, blank=True, null=True)   
    slug = models.SlugField(max_length=64, blank=True, null=True)

    title = models.CharField(max_length=64)   
    description = models.TextField(max_length=512, blank=True, null=True)
    
    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(Category, self).save(*args, **kwargs)

    
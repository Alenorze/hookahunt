from django.db import models



def remove_null(d):
    if not isinstance(d, (dict, list)):
        return d
    if isinstance(d, list):
        return [v for v in (remove_null(v) for v in d) if v]
    return {
        k: v
        for k, v in (
            (k, remove_null(v))
            for k, v in d.items()
        )
        if v is not None
    }


class RemoveNullFieldsMixin:
    def to_representation(self, instance):
        rep = super(RemoveNullFieldsMixin, self).to_representation(instance)
        return remove_null(rep)



class MetaModelMixin(models.Model):
    """ Model for meta fields. """
    seo_title = models.CharField(max_length=128, blank=True, null=True)
    seo_descrtiption = models.CharField(max_length=512, blank=True, null=True)
    keywords = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        abstract = True

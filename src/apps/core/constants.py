from model_utils import Choices


# Profile country
COUNTRY = Choices('Ukraine', 'Russia', 'Belarus', 'Poland')

# Article status
ARTICLE_STATUS = Choices('Created', 'Draft', 'Consideration', 'Published')

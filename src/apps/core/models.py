from django.db import models

from versatileimagefield.fields import PPOIField

from core.utils import image_path



class BaseModel(models.Model):
    """ Basic Model. """
    # A timestamp representing when this object was created.
    created_at = models.DateTimeField(auto_now_add=True)

    # A timestamp reprensenting when this object was last updated.
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        # By default, any model that inherits from `BaseModel` should
        # be ordered in reverse-chronological order. We can override this on a
        # per-model basis as needed, but reverse-chronological is a good
        # default ordering for most models.
        ordering = ['-created_at', '-updated_at']


class BaseImage(models.Model):
    """ Basic model for images. """
    title = models.CharField(max_length=200, null=True, blank=True)
    alt = models.CharField(max_length=200, null=True, blank=True)
    image_ppoi = PPOIField()

    class Meta:
        abstract = True
        verbose_name = "Image"
        verbose_name_plural = "Images"
    
    def __str__(self):
        res = ''
        if self.title:
            res = self.title
        else :
            res = self.image.url
        return res

from django.db import models
from django.conf import settings

from core.models import BaseModel




class Comment(BaseModel):

    body = models.TextField()
    
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name= "Author",    # One of each admins 
        on_delete=models.CASCADE
    )
    
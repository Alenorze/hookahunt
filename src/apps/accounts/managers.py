from core.managers import BaseManager, BaseQuerySet


class AccountQuerySet(BaseQuerySet):
    """ Account query set. """
    def active(self):
        return self.filter(is_active=True)


class AccountManager(BaseManager):
    """
    The account manager using it can easily change the queryset.

    """
    def get_query_set(self):
        return AccountQuerySet(self.model)
    
    def all(self):
        return self.get_queryset()

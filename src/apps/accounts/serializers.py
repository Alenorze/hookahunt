from rest_framework import serializers

from .models import Account

class AccounteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        # fields = (
        #     'username',
        #     'pro',
        #     'bio',
        #     'following'
        # )

from django.db import models
from django.contrib.auth.models import AbstractUser

from versatileimagefield.fields import VersatileImageField

from core.constants import COUNTRY
from core.models import BaseImage
from core.utils import image_path
from .managers import AccountManager



class AccountImage(BaseImage):
    source = VersatileImageField(
        upload_to='images/accounts/',
        ppoi_field='image_ppoi',
        null=True
    )
    
    class Meta:
        verbose_name = 'Article Preview'
        verbose_name_plural = 'Article Previews'


class Account(AbstractUser):
    """ Main account for all services. """
    profile_photo = models.OneToOneField(AccountImage, on_delete=models.CASCADE, blank=True, null=True)
    country = models.CharField(choices=COUNTRY, default=COUNTRY.Ukraine, max_length=16)
    bio = models.TextField(max_length=512)

    class Meta:
        verbose_name = 'Account'
        verbose_name_plural = 'Accounts'


class Language(models.Model):
    """ Languages ​​selected by the user """
    english = models.BooleanField(default=True)
    chinese = models.BooleanField(default=False)
    russian = models.BooleanField(default=False)
    japanese = models.BooleanField(default=False)
    korean = models.BooleanField(default=False)
    spanish = models.BooleanField(default=False)
    french = models.BooleanField(default=False)
    german = models.BooleanField(default=False)
    portuguese = models.BooleanField(default=False)
    italian = models.BooleanField(default=False)
    turkish = models.BooleanField(default=False)
    bahasa_indonesia = models.BooleanField(default=False)
    arabic = models.BooleanField(default=False)
    account = models.OneToOneField(Account, on_delete=models.CASCADE)

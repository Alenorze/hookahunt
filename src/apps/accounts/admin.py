from django.contrib import admin

from .models import Account, AccountImage



@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    search_fields = ['email']

admin.site.register(AccountImage)

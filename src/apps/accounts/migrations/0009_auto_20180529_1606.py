# Generated by Django 2.0.5 on 2018-05-29 16:06

from django.db import migrations
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0008_auto_20180529_0914'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountimage',
            name='source',
            field=versatileimagefield.fields.VersatileImageField(null=True, upload_to='images/accounts/'),
        ),
    ]

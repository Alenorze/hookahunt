from django.contrib.auth import get_user_model

from rest_framework import generics, permissions, viewsets
from .serializers import ProfileSerializer, UserSerializer
from rest_social_auth.views import JWTAuthMixin

from .models import Account
from core.permissions import IsModerator



class AccountView(generics.RetrieveUpdateAPIView, JWTAuthMixin):
    """ Displays the current user profile """
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ProfileSerializer
    model = get_user_model()

    def get_object(self, queryset=None):
        return self.request.user


class AccountViewSet(viewsets.ModelViewSet):
    """
    Displaying the list of all users also access to their editing,
    usually used for the moderator
    
    """
    # permission_classes = [IsModerator]
    serializer_class = AccountSerializer
    queryset = Account.objects.all()

from rest_framework import mixins, viewsets

from .models import Tag
from .serializers import TagSerializer
from core.permissions import IsOwnerOrReadOnly



class TagViewSet(mixins.ListModelMixin,
                    viewsets.GenericViewSet):
    serializer_class = TagSerializer
    queryset = Tag.objects.all()
    
from django.db import models
from django.db.models import permalink
from django.utils.text import slugify

from core.models import BaseModel


class Tag(models.Model):
    name = models.CharField(max_length=64, unique=True)    

    def __str__(self):
        return self.name
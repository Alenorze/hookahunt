from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView
from django.contrib.staticfiles.views import serve
from django.conf.urls.static import static
from django.conf import settings

from rest_framework import routers
from rest_framework.documentation import include_docs_urls

from posts.views import ArticleViewSet
from tags.views import TagViewSet

router = routers.DefaultRouter()
router.register(r'articles', ArticleViewSet)
router.register(r'tags', TagViewSet)



urlpatterns = [
    path('api/', include(router.urls)),
    re_path(r'^(?!/?static/)(?!/?media/)(?P<path>.*\..*)$',
    RedirectView.as_view(url='/static/%(path)s', permanent=False)),  
    path('admin/', admin.site.urls),
    # path('jet/', include('jet.urls', 'jet')),
    # path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    path('docs/', include_docs_urls(title='HookaHunt Docs')),
    re_path(r'^.*$', serve, kwargs={'path': 'index.html'})
]

urlpatterns += router.urls

# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

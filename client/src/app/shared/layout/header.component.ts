import { Component, OnInit } from '@angular/core';

import { User, UserService } from '../../core';

@Component({
  selector: 'app-layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  constructor(
    private userService: UserService
  ) {}

  currentUser: User;

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );

    // $( '.search' ).hide();
    const searchField = $('.search');
    const searchInput = $('input[type=\'search\']');

    const checkSearch = function() {
      const contents = searchInput.val();
        if (contents) {
          searchField.addClass('full');
        } else {
          searchField.removeClass('full');
        }
    };

    $('input[type=\'search\']').focus(function() {
        searchField.addClass('isActive');
      }).blur(function() {
        searchField.removeClass('isActive');
        checkSearch();
    });

    var textHolder = document.getElementById('neon'),
      text = textHolder.innerHTML,
      chars = text.length,
      newText = '';

    var letters = document.getElementsByTagName('i'),
      flickers = [5, 7, 9, 11, 13, 15, 17],
      randomLetter,
      flickerNumber,
      counter;

    function randomFromInterval(from,to) {
      return Math.floor(Math.random()* (to-from+1)+from);
    }

    function hasClass(element, cls) {
        return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
    }

    function flicker() {
      counter += 1;

      if (counter === flickerNumber) {
        return;
      }

      setTimeout(function () {
        if(hasClass(randomLetter, 'off')) {
          randomLetter.className = '';
        }
        else {
          randomLetter.className = 'off';
        }

        flicker();
      }, 30);
    }

    (function loop() {
        let rand = randomFromInterval(500, 3000);

      randomLetter = randomFromInterval(0, 3);
      randomLetter = letters[randomLetter];

      flickerNumber = randomFromInterval(0, 6);
      flickerNumber = flickers[flickerNumber];

        setTimeout(function() {
                counter = 0;
                flicker();
                loop();
        }, rand);
    }());
  }
}

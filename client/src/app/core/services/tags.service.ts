import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiService } from './api.service';
import { map } from 'rxjs/operators/map';

import { Tag } from '../models';



@Injectable()
export class TagsService {
  constructor (
    private apiService: ApiService
  ) {}



  getAll(): Observable<Tag> {
    return this.apiService.get('/tags/');
  }
}

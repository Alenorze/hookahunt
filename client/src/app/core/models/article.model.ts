import { Profile } from './profile.model';
import { Image } from './image.model';



export interface Article {
  id: number;
  author: Profile;
  image: Image;
  tags: string[];
  updated_at: string;
  published_at: string;
  slug: string;
  favorited: boolean;
  favoritesCount: number;
  seo_title: string;
  seo_descrtiption: string;
  keywords: string;

  title: string;
  description: string;
  body: string;
  category: string;
}

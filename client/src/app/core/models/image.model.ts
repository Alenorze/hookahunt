export interface Image {
  image: {
    source: {
      full: string;
      thumb: string;
    };
  title: string;
  alt: string;
  };
}
